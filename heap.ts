export class Heap<T> implements Iterable<T> {
    prior: (a: T, b: T) => boolean;
    equals: (a: T, b: T) => boolean;
    data: T[];
    dataIndices: Map<T, number>;

    constructor(priority_fn: (a: T, b: T) => boolean, members: Iterable<T>, equality_fn: (a: T, b: T) => boolean = (a, b) => a == b) {
        this.prior = priority_fn;
        this.equals = equality_fn;
        this.data = []
        this.dataIndices = new Map();
        for(let member of members) {
            this.push(member);
        }
    }

    push(item: T): number {
        this.data.push(item);
        let ind = this.data.length-1;
        return this.sift(ind) as number;
    }

    equalityBasedFind(item: T, index: number = 0): number | undefined {
        if(index < 0 || index >= this.data.length || this.prior(item, this.data[index])) {
            return;
        }
        if(this.equals(item, this.data[index])) {
            return index;
        }
        let leftChildIndex = 2*index+1
        let rightChildIndex = 2*index+2
        return this.equalityBasedFind(item, leftChildIndex) || this.equalityBasedFind(item, rightChildIndex);
    }

    find(item: T): number | undefined {
        return this.dataIndices.get(item) || this.equalityBasedFind(item);
    }

    has(item: T): boolean {
        return this.find(item) != undefined;
    }

    swap(i1: number, i2: number): void {
        let tmp = this.data[i1];
        this.data[i1] = this.data[i2];
        this.data[i2] = tmp;
    }

    siftUp(index: number): number | undefined {
        if(index == 0) {
            return 0;
        }
        if(index < 0 || index >= this.data.length) {
            return undefined;
        }
        let parentIndex = Math.floor((index-1)/2);
        if(this.prior(this.data[index], this.data[parentIndex])) {
            this.swap(index, parentIndex);
            return this.siftUp(parentIndex);
        }
        return index;
    }

    siftDown(index: number): number | undefined {
        if(index < 0 || index >= this.data.length) {
            return undefined;
        }
        let leftChildIndex = 2*index+1;
        let rightChildIndex = 2*index+2;
        if(leftChildIndex < this.data.length && this.prior(this.data[leftChildIndex], this.data[index])) {
            this.swap(index, leftChildIndex);
            return this.siftDown(leftChildIndex);
        } if(rightChildIndex < this.data.length && this.prior(this.data[rightChildIndex], this.data[index])) {
            this.swap(index, rightChildIndex);
            return this.siftDown(rightChildIndex);
        }
        return index;
    }

    sift(index: number): number | undefined {
        if(index < 0 || index >= this.data.length) {
            return undefined
        }
        let siftedIndex = this.siftUp(index) as number;
        let ret = (siftedIndex == index) ? this.siftDown(index) as number : siftedIndex;
        this.dataIndices.set(this.data[ret], ret);
        return ret;
    }

    delete(ind: number): T | undefined {
        if(ind < 0 || ind >= this.data.length) {
            return undefined;
        }

        let ret = this.data[ind];

        this.data[ind] = this.data[this.data.length-1];
        this.data = this.data.slice(0, this.data.length-1);

        this.sift(ind);

        return ret;
    }

    remove(item: T): boolean {
        let ind = this.find(item);
        if(ind == undefined) {
            return false;
        }
        this.delete(ind);
        return true;
    }

    pop(): T | undefined {
        return this.delete(0);
    }

    public [Symbol.iterator](): Iterator<T> {
        return {
            next: function(this: Heap<T>) {
                let next = this.pop();
                if(next == undefined) {
                    return {
                        done: true as true,
                        value: next,
                    }
                }
                return {
                    done: false as false,
                    value: next as T
                }
            }.bind(this)
        }
    }
}